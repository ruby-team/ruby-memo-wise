Source: ruby-memo-wise
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Cédric Boutillier <boutil@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb (>= 1)
Standards-Version: 4.6.0
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-memo-wise.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-memo-wise
Homepage: https://github.com/panorama-ed/memo_wise
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-memo-wise
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Multi-Arch: foreign
Description: wise Ruby memoization library
 MemoWise is a memoisation library with the following properties:
  * Fast performance of memoized reads
  * Support for resetting and presetting memoized values
  * Support for memoization on frozen objects
  * Support for memoization of class and module methods
  * Support for inheritance of memoized class and instance methods
  * Documented and tested thread-safety guarantees
